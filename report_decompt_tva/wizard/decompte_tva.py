# encoding: utf8
from openerp.osv import fields, osv
from openerp import netsvc
from datetime import datetime

####################################################"
##################Decompte TVA###################
####################################################"
class decompte_tva(osv.osv_memory):
    _name = "report.decompte.tva"
    _description = "Decompte TVA"
   
    _columns = {
        'date_begin': fields.date('Date début', required=True),
        'date_end': fields.date('Date fin', required=True),
        #'clt_exorene' :  fields.boolean('Afficher les clients exorénés de la TVA'),

           }
    
    def check_report(self, cr, uid, ids, context={}):
        data = self.read(cr,uid,ids,)[-1]   
        report_name ='jasper_report_decompte_tva'  
       
        return {
            'type'         : 'ir.actions.report.xml',
            'report_name'   : report_name,
            'datas': {
                    'model':'report.decompte.tva',
                    'id': context.get('active_ids') and context.get('active_ids')[0] or False,
                    'ids': context.get('active_ids') and context.get('active_ids') or [],
                    'parameters':data,
                  
                },
            'nodestroy': False
            }




    _defaults = {
    }
    
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
decompte_tva()
