{
    "name": "Rapport decompte TVA",
    "version": "1.0",
    "depends": ["base", 'account_voucher', 'account', 'account_followup','jasper_reports'],
    "author": "RCA",
    "category": "Rapport Decompte TVA",
    "description": """
    Ce module permet la génération du rapport decompte TVA 

    """,
    "init_xml": [],

    'data': [
             "wizard/decompte_tva.xml",
               
                   ],
    'demo_xml': [],
    'installable': True,
    'active': True,
}
